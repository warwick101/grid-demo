import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {


  @Input() parentForm: FormGroup;
  @Input() phoneValue = '(Optional)';

  business = [
    {value: 'Commercial'},
    {value: 'MMC'},
    {value: 'DSC'},
  ];

  drugs = [
    {value: 'Concerta'},
    {value: 'Myprodol'},
    {value: 'Derex'}
  ];

  quantities = [
    {value: 'EQ'},
    {value: 'OK'},
    {value: 'YES'}
  ];

  constructor(private fb: FormBuilder) {}

  ngOnInit() {
    this.parentForm = new FormGroup({
      'business': new FormControl('Commercial'),
      'drugs': new FormControl('Myprodol'),
      'auth': new FormControl('EQ'),
      'quantities': new FormControl('OK'),
    });
  }
}
